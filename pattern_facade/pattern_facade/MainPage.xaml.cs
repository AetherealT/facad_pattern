﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace pattern_facade
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void Convert(object sender, EventArgs eventArgs)
        {
            method method = new method();
            dollars dollars = new dollars();

            if (ruble.IsChecked)
            {
                dollars.set(double.Parse(entry_convert.Text));
                answer.Text = method.convert(entry_convert.Text).ToString();
            }
        }
    }
}
