﻿using System;
using System.Collections.Generic;
using System.Text;

namespace pattern_facade
{

    public class dollars
    {
        public double empty;

        public void set(double empty)
        {
            this.empty = empty;
        }

    }

    public class method
    {
        public double convert(object label)
        {
            dollars dollars = new dollars();
            double dollar = 80;
            dollars.set(double.Parse(label.ToString()));
            double answ =  dollar * dollars.empty;
            label = answ.ToString();
            return double.Parse(label.ToString());
        }
    }
}
